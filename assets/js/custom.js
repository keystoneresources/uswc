$(document).ready(function() {
    
    if($('body.corporate.type-12').length){
    
    	
    	$('#map_canvas').gmap().bind('init', function(ev, map) {
				$("[data-gmapping]").each(function(i,el) {
					var data = $(el).data('gmapping');
					$('#map_canvas').gmap('addMarker', {'id': data.id, 'tags':data.tags, 'position': new google.maps.LatLng(data.latlng.lat, data.latlng.lng), 'bounds':true }, function(map,marker) {
						$(el).click(function() {
							$(marker).triggerEvent('click');
						});
					}).click(function() {
						$('#map_canvas').gmap('openInfoWindow', { 'content': $(el).find('.info-box').html() }, this);
					});
				});	
			});
	    
	    
    }//if
    
    if($('body.type-10').length){
	    var $children = $('.grid.views-col');
			for(var i = 0, l = $children.length; i < l; i += 3) {
      	$children.slice(i, i+3).wrapAll('<div class="col-lg-12"><div class="row"></div></div>');
			}
	     
	    
    }
     
}); // End document ready 