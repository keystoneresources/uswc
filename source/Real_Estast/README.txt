Theme Information
-------------------------------------------------------------------------------
Real Estast
- Theme Version: 7.x-1.0
- Released on: Sep 09, 2014
- Packaged on: Sep 09, 2014
- Compatible with Drupal 7.x
- Copyright (C) 2012-2014 WeebPal.com. All Rights Reserved.
- @license - GNU/GPL, http://www.gnu.org/licenses/gpl.html
- Author: WeebPal Team
- Website: http://www.weebpal.com
- Guide: http://weebpal.com/guides
- Twitter: http://twitter.com/weebpal
- Facebook: http://www.facebook.com/weebpal


Main Folder Structure
-------------------------------------------------------------------------------

- demo\real_estast-profile-7.x-1.0.zip: demo profile for Real Estast.
- documentation: assets and the full guide for Real Estast installation
- theme\real_estast-7.x-1.0.zip: Real Estast theme
- PSD files: Real Estast Full PSD files
- README.txt: quick information of Real Estast

Installation
-------------------------------------------------------------------------------
For the full guide, read documentation\index.html

